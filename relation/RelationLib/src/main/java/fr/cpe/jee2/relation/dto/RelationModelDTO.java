package fr.cpe.jee2.relation.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.cpe.jee2.relation.RelationTypesEnum;

import java.io.Serializable;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RelationModelDTO implements Serializable {

    private String id;
    private String idUser;
    private RelationTypesEnum type;
    private String idEntity;

    public RelationModelDTO(String id, String idUser, RelationTypesEnum type, String idEntity) {
        this.id = id;
        this.idUser = idUser;
        this.type = type;
        this.idEntity = idEntity;
    }

    public RelationModelDTO() {}

    @Override
    public String toString() {
        return "RelationModelDTO{" +
                ", id=" + id +
                ", idUser=" + idUser +
                ", type=" + type +
                ", idEntity='" + idEntity + '\'' +
                '}';
    }

    public String getId() {return id;}

    public void setId(String id) {this.id = id;}

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(String idEntity) {
        this.idEntity = idEntity;
    }

    public RelationTypesEnum getType() {
        return type;
    }

    public void setType(RelationTypesEnum type) {
        this.type = type;
    }
}
