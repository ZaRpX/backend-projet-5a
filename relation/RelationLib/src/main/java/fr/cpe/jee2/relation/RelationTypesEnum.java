package fr.cpe.jee2.relation;

public enum RelationTypesEnum {
    followEvent,
    followPromoter,
    friendship,
    friendshipRequest,
    testing
}
