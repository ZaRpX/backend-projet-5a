package fr.cpe.jee2.relation.service.test;

import com.github.npathai.hamcrestopt.OptionalMatchers;
import fr.cpe.jee2.event.dto.EventModelDTO;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import fr.cpe.jee2.relation.RelationTypesEnum;
import fr.cpe.jee2.relation.model.RelationModel;
import fr.cpe.jee2.relation.repository.RelationRepository;
import fr.cpe.jee2.relation.service.RelationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RelationServiceTest {

    @Mock
    RelationRepository relationRepository;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    @Spy
    RelationService relationService;

    @Test
    public void createRelation(){
        RelationModel relationModel = new RelationModel();
        relationModel.setId("test");
        relationModel.setIdEntity("test");

        //On test la v�rification de la pr�sence d'un utilisateur
        Mockito.when(relationRepository.findByIdUserAndTypeAndIdEntity(
                relationModel.getIdUser(),relationModel.getType(),relationModel.getIdEntity())).thenReturn(relationModel);
        Assert.assertEquals(relationService.createRelation(relationModel), "alreadyExists");

        //On test si l'on trouve l' utilisateur
        Mockito.when(relationRepository.findByIdUserAndTypeAndIdEntity(
                relationModel.getIdUser(),relationModel.getType(),relationModel.getIdEntity())).thenReturn(null);
        Mockito.when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + relationModel.getIdUser(), GoerModelDTO.class)).thenReturn(null);
        Assert.assertEquals(relationService.createRelation(relationModel), "not found");

        //On v�rifie selon le type
        Mockito.when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + relationModel.getIdUser(), GoerModelDTO.class)).thenReturn(new GoerModelDTO());

        //Si l'event n'existe pas
        relationModel.setType(RelationTypesEnum.followEvent);
        Mockito.when(restTemplate.getForObject("https://event-dot-festsharev2.appspot.com/event/" + relationModel.getIdEntity(), EventModelDTO.class)).thenReturn(null);
        Assert.assertEquals(relationService.createRelation(relationModel), "not found");

        //Si le promoter n'existe pas
        relationModel.setType(RelationTypesEnum.followPromoter);
        Mockito.when(restTemplate.getForObject("https://promoter-dot-festsharev2.appspot.com/promoter/" + relationModel.getIdEntity(), PromoterModelDTO.class)).thenReturn(null);
        Assert.assertEquals(relationService.createRelation(relationModel), "not found");

        //Si l'ami n'existe pas
        relationModel.setType(RelationTypesEnum.friendship);
        Mockito.when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + relationModel.getIdEntity(), GoerModelDTO.class)).thenReturn(null);
        Assert.assertEquals(relationService.createRelation(relationModel), "not found");
        relationModel.setType(RelationTypesEnum.friendshipRequest);
        Mockito.when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + relationModel.getIdEntity(), GoerModelDTO.class)).thenReturn(null);
        Assert.assertEquals(relationService.createRelation(relationModel), "not found");

        //Autre type de relation
        relationModel.setType(RelationTypesEnum.testing);
        Assert.assertEquals(relationService.createRelation(relationModel), "wrong type");

        //On test la sauvegarde

        relationModel.setType(RelationTypesEnum.friendship);
        Mockito.when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + relationModel.getIdEntity(), GoerModelDTO.class)).thenReturn(new GoerModelDTO());
        Mockito.when(relationRepository.save(relationModel)).thenReturn(relationModel);
        Assert.assertEquals(relationService.createRelation(relationModel), relationModel.toString());
    }

    @Test
    public void getRelationByGoer(){
        String idGoer = "test";
        RelationTypesEnum relationTypesEnum = RelationTypesEnum.friendship;

        List relationList = new ArrayList<RelationModel>();
        relationList.add(new RelationModel());

        //On test si le goer existe pas
        Mockito.when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + idGoer, GoerModelDTO.class)).thenReturn(null);
        Assert.assertNull(relationService.getRelationByGoer(idGoer, relationTypesEnum));

        //On verifie si l'on trouve bien des relation
        Mockito.when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + idGoer, GoerModelDTO.class)).thenReturn(new GoerModelDTO());
        Mockito.when(relationRepository.findByIdUserAndType(idGoer, relationTypesEnum)).thenReturn(relationList);
        Mockito.when(relationRepository.findByIdEntityAndType(idGoer, relationTypesEnum)).thenReturn(relationList);
        Assert.assertEquals(relationService.getRelationByGoer(idGoer, relationTypesEnum).size(), 2);

    }

    @Test
    public void getRelationByEntity(){
        String idEntity = "test";
        RelationTypesEnum relationTypesEnum = RelationTypesEnum.friendship;

        List relationList = new ArrayList<RelationModel>();
        relationList.add(new RelationModel());

        //On verifie que l'on trouve bien les deux relations
        Mockito.when(relationRepository.findByIdEntityAndType(idEntity, relationTypesEnum)).thenReturn(relationList);
        Mockito.when(relationRepository.findByIdUserAndType(idEntity, relationTypesEnum)).thenReturn(relationList);
        Assert.assertEquals(relationService.getRelationByEntity(idEntity, relationTypesEnum).size(), 2);
    }

    @Test
    public void removeRelation(){
        String id = "test";
        RelationModel relationModel = new RelationModel();
        relationModel.setId(id);
        Optional<RelationModel> optionalRelation = Optional.of(relationModel);

        //On mock les m�thodes de recherche/suppression
        Mockito.when(relationRepository.findById(id)).thenReturn(optionalRelation);
        Mockito.doNothing().when(relationRepository).delete(relationModel);
        Assert.assertEquals(relationService.removeRelation(id), id);
    }

    @Test
    public void getAllRelation(){
        //Aucune logique m�tier, on verifie juste la bonne execution
        List<RelationModel> arrayList = new ArrayList<>();
        Mockito.when(relationRepository.findAll()).thenReturn(arrayList);
        Assert.assertEquals(relationService.getAllRelations(),arrayList);
    }

    @Test
    public void getRelation(){
        //Aucune logique m�tier, on verifie juste la bonne execution
        String id = "test";
        RelationModel eventModel = new RelationModel();
        Mockito.when(relationRepository.findById(id)).thenReturn(Optional.of(eventModel));
        Assert.assertThat(relationService.getRelation(id), OptionalMatchers.hasValue(eventModel));
    }
}


