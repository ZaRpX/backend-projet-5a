package fr.cpe.jee2.relation.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.relation.RelationTypesEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection ="relation")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class RelationModel implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private String idUser;
	private RelationTypesEnum type;
	private String idEntity;

	private transient GoerModelDTO user;
	private transient Object entity;

	public RelationModel(String idUser, RelationTypesEnum type, String idEntity) {
		this.idUser = idUser;
		this.type = type;
		this.idEntity = idEntity;
	}

	public RelationModel() {}

	@Override
	public String toString() {
		return "RelationModel{" +
				"id='" + id + '\'' +
				", idUser='" + idUser + '\'' +
				", type=" + type +
				", idEntity='" + idEntity + '\'' +
				", user=" + user +
				", entity=" + entity +
				'}';
	}

	public String getId() {return id;}

	public void setId(String id) {this.id = id;}

	public String getIdUser() {return idUser;}

	public void setIdUser(String idUser) {this.idUser = idUser;}

	public String getIdEntity() {return idEntity;}

	public void setIdEntity(String idEntity) {this.idEntity = idEntity;}

	public RelationTypesEnum getType() {
		return type;
	}

	public void setType(RelationTypesEnum type) {
		this.type = type;
	}

	public GoerModelDTO getUser() {
		return user;
	}

	public void setUser(GoerModelDTO user) {
		this.user = user;
	}

	public Object getEntity() {
		return entity;
	}

	public void setEntity(Object entity) {
		this.entity = entity;
	}
}
