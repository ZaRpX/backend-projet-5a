package fr.cpe.jee2.relation.service;
import fr.cpe.jee2.event.dto.EventModelDTO;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import fr.cpe.jee2.relation.RelationTypesEnum;
import fr.cpe.jee2.relation.model.RelationModel;
import fr.cpe.jee2.relation.repository.RelationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class RelationService {

    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private RelationRepository relationRepository;

    public String createRelation( RelationModel relation) {

        RelationModel verif = relationRepository.findByIdUserAndTypeAndIdEntity(relation.getIdUser(),relation.getType(),relation.getIdEntity());

        if (verif != null)
            return "alreadyExists";

        //Contrôle de l'existence du user
        String goerUrl = "https://goer-dot-festsharev2.appspot.com/goer/" + relation.getIdUser();

        GoerModelDTO responseGoer = restTemplate.getForObject(
                goerUrl,
                GoerModelDTO.class);

        if (responseGoer==null)
            return "not found";

        //Contrôle de l'existence du topic selon le type
        switch (relation.getType()){
            case followEvent:
                String eventUrl = "https://event-dot-festsharev2.appspot.com/event/" + relation.getIdEntity();

                EventModelDTO responseEvent= restTemplate.getForObject(
                        eventUrl,
                        EventModelDTO.class);

                if (responseEvent==null)
                    return "not found";
                break;
            case followPromoter:
                String promoterUrl = "https://promoter-dot-festsharev2.appspot.com/promoter/" + relation.getIdEntity();

                PromoterModelDTO responsePromoter = restTemplate.getForObject(
                        promoterUrl,
                        PromoterModelDTO.class);

                if (responsePromoter==null)
                    return "not found";
                break;
            case friendship:
                //Pas de break, c'est la même chose que pour request
            case friendshipRequest:
                String goerUrlBis = "https://goer-dot-festsharev2.appspot.com/goer/" + relation.getIdEntity();

                GoerModelDTO responseGoerBis = restTemplate.getForObject(
                        goerUrlBis,
                        GoerModelDTO.class);

                if (responseGoerBis==null)
                    return "not found";
                break;
            default:
                return "wrong type";
        }


        try {
            relationRepository.save(relation);
            return relation.toString();
        } catch (Exception e) {
            return "error";
        }
    }

    public List<RelationModel> getAllRelations() {
        return new ArrayList<>(relationRepository.findAll());
    }

    public Optional<RelationModel> getRelation(String id) {
        return relationRepository.findById(id);
    }

    public List<RelationModel> getRelationByGoer(String idGoer, RelationTypesEnum type) {

        //Contrôle de l'existence du goer
        String goerUrl = "https://goer-dot-festsharev2.appspot.com/goer/" + idGoer;

        GoerModelDTO responseGoer = restTemplate.getForObject(
                goerUrl,
                GoerModelDTO.class);

        if (responseGoer==null)
            return null;

        List<RelationModel> arrayList = new ArrayList<>(relationRepository.findByIdUserAndType(idGoer, type));
        if (type==RelationTypesEnum.friendship){
            arrayList.addAll(relationRepository.findByIdEntityAndType(idGoer, type));
        }
        return arrayList;
    }

    public List<RelationModel> getRelationByEntity(String idEntity, RelationTypesEnum type) {
        //TODO : Controle que l'entity existe

        List<RelationModel> arrayList = new ArrayList<>(relationRepository.findByIdEntityAndType(idEntity, type));
        if (type==RelationTypesEnum.friendship){
            arrayList.addAll(relationRepository.findByIdUserAndType(idEntity, type));
        }
        return arrayList;
    }

    public String removeRelation(String id) {
        try {
            Optional<RelationModel> promoterModel = relationRepository.findById(id);
            relationRepository.delete(promoterModel.get());
            return id;
        } catch (Exception e) {
            return "error";
        }
    }

    public String unfollowEntity(RelationModel relationModel) {
        try {
            RelationModel promoterModel = relationRepository.findByIdUserAndTypeAndIdEntity(relationModel.getIdUser(), relationModel.getType(), relationModel.getIdEntity());
            relationRepository.delete(promoterModel);
            return promoterModel.getId();
        } catch (Exception e) {
            return "error";
        }
    }
}

