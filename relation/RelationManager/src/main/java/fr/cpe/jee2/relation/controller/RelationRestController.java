package fr.cpe.jee2.relation.controller;

import fr.cpe.jee2.relation.RelationTypesEnum;
import fr.cpe.jee2.relation.dto.RelationModelDTO;
import fr.cpe.jee2.relation.model.RelationModel;
import fr.cpe.jee2.relation.service.RelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class RelationRestController {

    @Autowired
    private RelationService relationService;

    @RequestMapping(method=RequestMethod.POST,value="/relation")
    public ResponseEntity<String> postRelation(@RequestBody RelationModel relation) {
        String result = relationService.createRelation(relation);
        switch (result){
            case "error":
                return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
            case "not found":
                return new ResponseEntity<>("entities not found", HttpStatus.SEE_OTHER);
            case "alreadyExists":
                return new ResponseEntity<>("relation already exists", HttpStatus.SEE_OTHER);
            case "wrong type":
                return new ResponseEntity<>("wrong parameter", HttpStatus.NOT_ACCEPTABLE);
            default:
                return new ResponseEntity<>(relation.toString(), HttpStatus.CREATED);

        }
    }

    @RequestMapping("/relations")
    private ResponseEntity<List<RelationModel>> getAllRelations() {
        return new ResponseEntity<>(relationService.getAllRelations(), HttpStatus.OK);
    }

    @RequestMapping("/relation/{id}")
    private ResponseEntity<RelationModelDTO> getRelation(@PathVariable String id) {
        Optional<RelationModel> relation;
        relation = relationService.getRelation(id);
        RelationModelDTO relationModelDTO = new RelationModelDTO(relation.get().getId(),relation.get().getIdUser(),relation.get().getType(),relation.get().getIdEntity());
        return new ResponseEntity<>(relationModelDTO, HttpStatus.OK);
    }

    @RequestMapping("/relation/goer/{idGoer}/{type}")
    private ResponseEntity<List<RelationModel>> getRelationByGoer(@PathVariable("idGoer") String idGoer, @PathVariable("type") RelationTypesEnum type) {
        return new ResponseEntity<>(relationService.getRelationByGoer(idGoer, type), HttpStatus.OK);
    }

    @RequestMapping("/relation/entity/{idEntity}/{type}")
    private ResponseEntity<List<RelationModel>> getRelationByEntity(@PathVariable("idEntity") String idEntity, @PathVariable("type") RelationTypesEnum type) {
        return new ResponseEntity<>(relationService.getRelationByEntity(idEntity, type), HttpStatus.OK);
    }


    @RequestMapping("/relation/test")
    private ResponseEntity<String> getTestMessage(){
        return new ResponseEntity<>("ok",HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.PUT, value="/relation/unfollow")
    public ResponseEntity<String> unfollowEntity(@RequestBody RelationModel relationModel){
        String result = relationService.unfollowEntity(relationModel);
        if(result.equals("error"))
            return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<>(result + "deleted", HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/relation/{id}")
    public ResponseEntity<String> deleteRelation(@PathVariable String id){
        String result = relationService.removeRelation(id);
        if(result.equals("error"))
            return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<>(id + "deleted", HttpStatus.OK);
    }
}
