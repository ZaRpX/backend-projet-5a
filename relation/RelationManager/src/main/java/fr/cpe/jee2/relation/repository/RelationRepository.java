package fr.cpe.jee2.relation.repository;

import fr.cpe.jee2.relation.RelationTypesEnum;
import fr.cpe.jee2.relation.model.RelationModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface RelationRepository extends MongoRepository<RelationModel, String> {

    public List<RelationModel> findByIdUserAndType(String idUser, RelationTypesEnum type);

    public List<RelationModel> findByIdEntityAndType(String idEntity, RelationTypesEnum type);

    public RelationModel findByIdUserAndTypeAndIdEntity(String idUser, RelationTypesEnum type, String idEntity);
}
