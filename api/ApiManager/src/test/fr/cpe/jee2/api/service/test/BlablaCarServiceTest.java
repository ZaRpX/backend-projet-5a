package fr.cpe.jee2.api.service.test;

import fr.cpe.jee2.api.service.BlablaCarService;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BlablaCarServiceTest {

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    @Spy
    BlablaCarService blablaCarService;

    @Value("${blablacar.key}")
    private String blablaCarKey;

    @Test
    public void getTrajets() {
        String depart = "test";
        String arrivee = "test";
        String apiUrl = "https://public-api.blablacar.com/api/v2/trips?fn="
                + depart + "&tn=" + arrivee +
                "&locale=fr_FR&key=" +  blablaCarKey;
        String exampleBlablaCarReturn = "{  \"links\":{  \"_self\":\"https:\\/\\/api.blablacar.fr\\/api\\/v2\\/trips\", \"_front\":\"https:\\/\\/www.blablacar.fr\\/search?fn=Grenoble&fc=45.188529%2C5.724524&tn=Lyon&tc=45.764043%2C4.835659\" }, \"pager\":{  \"page\":1, \"pages\":42, \"total\":419, \"limit\":10 }, \"trips\":[  {  \"links\":{  \"_self\":\"https:\\/\\/api.blablacar.fr\\/api\\/v2\\/trips\\/1901981541-noyarey-lyon\", \"_front\":\"https:\\/\\/www.blablacar.fr\\/trip\\/carpooling\\/1901981541-noyarey-lyon\\/?_force_https=1\" }, \"departure_date\":\"01\\/02\\/2020 18:00:00\", \"departure_place\":{  \"city_name\":\"Noyarey\", \"address\":\"Noyarey\", \"latitude\":45.243625, \"longitude\":5.632037, \"country_code\":\"FR\" }, \"arrival_place\":{  \"city_name\":\"Lyon\", \"address\":\"Place de la Bourse, Lyon\", \"latitude\":45.764594, \"longitude\":4.835857, \"country_code\":\"FR\" }, \"price\":{  \"value\":7, \"currency\":\"EUR\", \"symbol\":\"\\u20ac\", \"string_value\":\"7,00\\u00a0\\u20ac\", \"price_color\":\"BLACK\" }, \"price_with_commission\":{  \"value\":8.5, \"currency\":\"EUR\", \"symbol\":\"\\u20ac\", \"string_value\":\"8,50\\u00a0\\u20ac\", \"price_color\":\"BLACK\" }, \"price_without_commission\":{  \"value\":7, \"currency\":\"EUR\", \"symbol\":\"\\u20ac\", \"string_value\":\"7,00\\u00a0\\u20ac\", \"price_color\":\"BLACK\" }, \"commission\":{  \"value\":1.5, \"currency\":\"EUR\", \"symbol\":\"\\u20ac\", \"string_value\":\"1,50\\u00a0\\u20ac\" }, \"seats_left\":2, \"seats\":3, \"duration\":{  \"value\":6600, \"unity\":\"s\" }, \"distance\":{  \"value\":120, \"unity\":\"km\" }, \"permanent_id\":\"1901981541-noyarey-lyon\", \"car\":{  \"id\":\"38202913\", \"model\":\"SERIE 1\", \"make\":\"BMW\", \"comfort\":\"normal\", \"comfort_nb_star\":2, \"category\":\"CATEGORY_BERLINE\" }, \"multimodal_id\":{  \"source\":\"CARPOOLING\", \"id\":\"1901981541-noyarey-lyon\" }, \"viaggio_rosa\":false, \"is_comfort\":true, \"freeway\":true, \"answer_delay\":0, \"booking_mode\":\"auto\", \"booking_type\":\"online\", \"locations_to_display\":[  \"Noyarey\", \"Lyon\" ], \"departure_passenger_routing\":{  \"proximity\":\"MIDDLE\", \"routes\":[  {  \"distance_in_meters\":9488, \"type\":\"AS_THE_CROW_FLIES\" } ] }, \"arrival_passenger_routing\":{  \"proximity\":\"CLOSE\", \"routes\":[  {  \"distance_in_meters\":63, \"type\":\"AS_THE_CROW_FLIES\" } ] }, \"corridoring_type\":\"PLANNED\", \"vehicle_pictures\":[   ] } ], \"top_trips\":[   ], \"facets\":[  {  \"name\":\"hour\", \"type\":\"custom\", \"items\":[  {  \"value\":5, \"count\":1, \"is_selected\":true }, {  \"value\":6, \"count\":1, \"is_selected\":false }, {  \"value\":7, \"count\":1, \"is_selected\":false }, {  \"value\":8, \"count\":1, \"is_selected\":false }, {  \"value\":9, \"count\":1, \"is_selected\":false }, {  \"value\":10, \"count\":1, \"is_selected\":false }, {  \"value\":11, \"count\":1, \"is_selected\":false }, {  \"value\":12, \"count\":1, \"is_selected\":false }, {  \"value\":13, \"count\":1, \"is_selected\":false }, {  \"value\":14, \"count\":1, \"is_selected\":false }, {  \"value\":15, \"count\":1, \"is_selected\":false }, {  \"value\":16, \"count\":1, \"is_selected\":false }, {  \"value\":17, \"count\":1, \"is_selected\":false }, {  \"value\":18, \"count\":1, \"is_selected\":false }, {  \"value\":19, \"count\":1, \"is_selected\":false }, {  \"value\":20, \"count\":1, \"is_selected\":false }, {  \"value\":21, \"count\":1, \"is_selected\":false }, {  \"value\":22, \"count\":1, \"is_selected\":false }, {  \"value\":23, \"count\":1, \"is_selected\":false }, {  \"value\":24, \"count\":1, \"is_selected\":true } ] }, {  \"name\":\"price\", \"type\":\"custom\", \"items\":[  {  \"value\":5, \"count\":1, \"is_selected\":true }, {  \"value\":6, \"count\":1, \"is_selected\":false }, {  \"value\":7, \"count\":1, \"is_selected\":false }, {  \"value\":8, \"count\":1, \"is_selected\":false }, {  \"value\":9, \"count\":1, \"is_selected\":false }, {  \"value\":10, \"count\":1, \"is_selected\":false }, {  \"value\":11, \"count\":1, \"is_selected\":false }, {  \"value\":12, \"count\":1, \"is_selected\":false }, {  \"value\":13, \"count\":1, \"is_selected\":false }, {  \"value\":14, \"count\":1, \"is_selected\":false }, {  \"value\":15, \"count\":1, \"is_selected\":false }, {  \"value\":16, \"count\":1, \"is_selected\":false }, {  \"value\":17, \"count\":1, \"is_selected\":false }, {  \"value\":18, \"count\":1, \"is_selected\":false }, {  \"value\":19, \"count\":1, \"is_selected\":false }, {  \"value\":20, \"count\":1, \"is_selected\":false }, {  \"value\":21, \"count\":1, \"is_selected\":false }, {  \"value\":22, \"count\":1, \"is_selected\":false }, {  \"value\":23, \"count\":1, \"is_selected\":false }, {  \"value\":24, \"count\":1, \"is_selected\":false }, {  \"value\":25, \"count\":1, \"is_selected\":false }, {  \"value\":26, \"count\":1, \"is_selected\":false }, {  \"value\":27, \"count\":1, \"is_selected\":false }, {  \"value\":28, \"count\":1, \"is_selected\":false }, {  \"value\":29, \"count\":1, \"is_selected\":false }, {  \"value\":30, \"count\":1, \"is_selected\":false }, {  \"value\":31, \"count\":1, \"is_selected\":false }, {  \"value\":32, \"count\":1, \"is_selected\":false }, {  \"value\":33, \"count\":1, \"is_selected\":false }, {  \"value\":34, \"count\":1, \"is_selected\":true } ] }, {  \"name\":\"automatic_approval\", \"type\":\"checkbox\", \"items\":[  {  \"value\":true, \"count\":190, \"is_selected\":false } ] } ], \"distance\":112, \"duration\":4800, \"recommended_price\":6, \"savings\":18, \"lowest_price\":7, \"lowest_price_object\":{  \"value\":7.5, \"currency\":\"EUR\", \"symbol\":\"\\u20ac\", \"string_value\":\"7,50\\u00a0\\u20ac\" }, \"full_trips_count\":74, \"total_trip_count_to_display\":419, \"sorting_algorithm\":\"trip_date\", \"has_bus\":false }";
        Mockito.when(restTemplate.getForObject(apiUrl, String.class)).thenReturn(exampleBlablaCarReturn);
        Assert.assertThat(blablaCarService.getTrajets(depart, arrivee),
                CoreMatchers.instanceOf(ArrayList.class));
    }
}
