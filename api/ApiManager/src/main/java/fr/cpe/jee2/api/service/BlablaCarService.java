package fr.cpe.jee2.api.service;

import fr.cpe.jee2.api.Model.BlablaCarTripModel;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class BlablaCarService {

    @Value("${blablacar.key}")
    private String blablaCarKey;

    RestTemplate restTemplate = new RestTemplate();

    public List<BlablaCarTripModel> getTrajets(String depart, String arrivee) {

        String apiUrl = "https://public-api.blablacar.com/api/v2/trips?fn="
                + depart + "&tn=" + arrivee +
                "&locale=fr_FR&key=" +  blablaCarKey;

        String responseJson = restTemplate.getForObject(apiUrl, String.class);
        if (responseJson != null) {
            try {
                JSONParser parser = new JSONParser(responseJson);
                Map<String,Object> map = parser.parseObject();
                if (map.containsKey("trips")) {
                    List<BlablaCarTripModel> results = new ArrayList<>();
                    List<Map<String, Map<String,Object>>> trips = (List<Map<String, Map<String,Object>>>) map.get("trips");
                    DateFormat dt = new SimpleDateFormat("dd\\/MM\\/yyyy HH:mm:ss");
                    for (Map<String, Map<String,Object>> trip : trips) {
                            results.add(new BlablaCarTripModel(
                                    trip.get("departure_place").get("address").toString(),
                                    trip.get("arrival_place").get("address").toString(),
                                    dt.parse(String.valueOf(trip.get("departure_date"))),
                                    ((Number)trip.get("price_with_commission").get("value")).doubleValue()));
                    };
                    return results;
                }
                return new ArrayList<>();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
