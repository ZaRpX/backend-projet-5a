package fr.cpe.jee2.api.controller;

import fr.cpe.jee2.api.Model.BlablaCarTripModel;
import fr.cpe.jee2.api.service.BlablaCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class BlablaCarRestController {

    @Autowired
    private BlablaCarService blablaCarService;

    @RequestMapping("/api/blablacar/{depart}/{arrivee}")
    private ResponseEntity<List<BlablaCarTripModel>> getTrajets(@PathVariable String depart, @PathVariable String arrivee) {
        return new ResponseEntity<>(blablaCarService.getTrajets(depart,arrivee), HttpStatus.OK);
    }

}
