package fr.cpe.jee2.api.Model;

import java.io.Serializable;
import java.util.Date;

public class BlablaCarTripModel implements Serializable {

    private String depart;
    private String arrivee;
    private Date date;
    private Double prix;

    public BlablaCarTripModel(String depart, String arrivee, Date date, Double prix) {
        this.depart = depart;
        this.arrivee = arrivee;
        this.date = date;
        this.prix = prix;
    }

    public BlablaCarTripModel() {
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
}
