package fr.cpe.jee2.goer.repository;

import fr.cpe.jee2.goer.model.GoerModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoerRepository extends MongoRepository<GoerModel, String> {

}
