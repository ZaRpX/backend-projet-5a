package fr.cpe.jee2.goer.service;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cpe.jee2.event.dto.EventModelDTO;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.goer.model.GoerModel;
import fr.cpe.jee2.goer.repository.GoerRepository;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import fr.cpe.jee2.relation.RelationTypesEnum;
import fr.cpe.jee2.relation.dto.RelationModelDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Service
public class GoerService {

    @Autowired
    private GoerRepository goerRepository;

    RestTemplate restTemplate = new RestTemplate();

    public String createGoer( GoerModel goer) {

        //Contrôle de nom non vide
        if (goer.getName().isEmpty())
            return "empty";

        //Contrôle du format de l'adresse mail
        try {
            InternetAddress emailAddr = new InternetAddress(goer.getMail());
            emailAddr.validate();
        } catch (AddressException ex) {
            return "wrong mail";
        }

        //Contrôle de description non vide
        if (goer.getDescription().isEmpty())
            return "empty";


        try {
            goerRepository.save(goer);
            return goer.toString();
        } catch (Exception e) {
            return "error";
        }
    }

    public List<GoerModel> getAllGoers() {return goerRepository.findAll();}

    public Optional<GoerModel> getGoerById(String id) {return goerRepository.findById(id);}

    public List<GoerModel> getGoersFriends(String idGoer) {

        if(!getGoerById(idGoer).isPresent()) return null;

        ObjectMapper mapper = new ObjectMapper();
        List<GoerModel> resultList = new ArrayList<>();

        //Appel au microservice relation
        String goerUrl = "https://relation-dot-festsharev2.appspot.com/relation/goer/" + idGoer + "/friendship";

        //On récupère la liste en JSon
        String responseRelation = restTemplate.getForObject(goerUrl, String.class);

        try {
            // On la transforme en objet métier
            //noinspection RedundantCollectionOperation
            List<RelationModelDTO> listRelation = Arrays.asList(mapper.readValue(responseRelation, RelationModelDTO[].class));

            //On vérifie que les relations sont des amitiées et on les récupères
            for (RelationModelDTO relation : listRelation) {
                if(relation.getType().equals(RelationTypesEnum.friendship)) {
                    Optional<GoerModel> e = goerRepository.findById(relation.getIdEntity());
                    e.ifPresent(resultList::add);
                }
            }

            return resultList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String removeGoer(String id) {
        try {
            Optional<GoerModel> goerModel = goerRepository.findById(id);
            goerRepository.delete(goerModel.get());
            return id;
        } catch (Exception e) {
            return "error";
        }
    }

    public List<GoerModel> getGoersByEvent(String idEvent) {

        ObjectMapper mapper = new ObjectMapper();
        List<GoerModel> resultList = new ArrayList<>();

        //Contrôle de l'existence de l'event
        String eventUrl = "https://event-dot-festsharev2.appspot.com/event/" + idEvent;

        EventModelDTO responseEvent = restTemplate.getForObject(
                eventUrl,
                EventModelDTO.class);

        if (responseEvent==null)
            return null;

        //Appel au microservice relation
        String relationUrl = "https://relation-dot-festsharev2.appspot.com/relation/entity/" + idEvent + "/followEvent";

        String responseRelation = restTemplate.getForObject(
                relationUrl,
                String.class);

        try {
            //convert JSON array to List of objects
            List<RelationModelDTO> test2 = Arrays.asList(mapper.readValue(responseRelation, RelationModelDTO[].class));

            test2.forEach(x -> {
                Optional<GoerModel> e = goerRepository.findById(x.getIdUser());
                resultList.add(e.get());
            });
            return resultList;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<GoerModel> getGoersByPromoter(String idPromoter) {

        ObjectMapper mapper = new ObjectMapper();
        List<GoerModel> resultList = new ArrayList<>();

        //Contrôle de l'existence du promoter
        String promoterUrl = "https://promoter-dot-festsharev2.appspot.com/promoter/" + idPromoter;

        PromoterModelDTO responsePromoter = restTemplate.getForObject(
                promoterUrl,
                PromoterModelDTO.class);

        if (responsePromoter==null)
            return null;

        //Appel au microservice relation
        String relationUrl = "https://relation-dot-festsharev2.appspot.com/relation/entity/" + idPromoter + "/followPromoter";

        String responseRelation = restTemplate.getForObject(
                relationUrl,
                String.class);

        try {
            //convert JSON array to List of objects
            List<RelationModelDTO> test2 = Arrays.asList(mapper.readValue(responseRelation, RelationModelDTO[].class));

            test2.forEach(x -> {
                Optional<GoerModel> e = goerRepository.findById(x.getIdUser());
                resultList.add(e.get());
            });
            return resultList;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

