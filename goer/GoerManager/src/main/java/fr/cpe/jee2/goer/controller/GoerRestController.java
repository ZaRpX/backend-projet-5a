package fr.cpe.jee2.goer.controller;

import fr.cpe.jee2.goer.model.GoerModel;
import fr.cpe.jee2.goer.service.GoerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class GoerRestController {

    @Autowired
    private GoerService goerService;

    @RequestMapping(method=RequestMethod.POST,value="/goer")
    public ResponseEntity<String> postGoer(@RequestBody GoerModel goer) {
        String result = goerService.createGoer(goer);

        switch (result){
            case "error":
                return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
            case "wrong mail":
                return new ResponseEntity<>("wrong mail format", HttpStatus.NOT_ACCEPTABLE);
            case "empty":
                return new ResponseEntity<>("missing parameter", HttpStatus.NO_CONTENT);
            default:
                return new ResponseEntity<>(goer.toString(), HttpStatus.CREATED);

        }
    }

    @RequestMapping("/goers")
    private ResponseEntity<List<GoerModel>> getAllGoers() {
        return new ResponseEntity<>(goerService.getAllGoers(), HttpStatus.OK);
    }


    @RequestMapping("/goer/{id}")
    private ResponseEntity<GoerModel> getGoerById(@PathVariable String id) {
        Optional<GoerModel> goer;
        goer = goerService.getGoerById(id);
        return new ResponseEntity<>(goer.orElse(null), HttpStatus.OK);
    }

    @RequestMapping("/goer/{id}/friends")
    private ResponseEntity<List<GoerModel>> getGoersFriends(@PathVariable String id) {
        List<GoerModel> goers;
        goers = goerService.getGoersFriends(id);
        if(goers == null)
            return new ResponseEntity<>(null, HttpStatus.SEE_OTHER);
        else
        return new ResponseEntity<>(goers, HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/goer/{id}")
    public ResponseEntity<String> deleteGoer(@PathVariable String id){
        String result = goerService.removeGoer(id);
        if(result.equals("error"))
            return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<>(id + "deleted", HttpStatus.OK);
    }

    @RequestMapping("/goers/event/{idEvent}")
    private ResponseEntity<List<GoerModel>> getGoersByEvent(@PathVariable("idEvent") String idEvent) {
        List<GoerModel> result;
        result = goerService.getGoersByEvent(idEvent);
        if (result == null) {
            return new ResponseEntity<>(null, HttpStatus.SEE_OTHER);
        } else {
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

    @RequestMapping("/goers/promoter/{idPromoter}")
    private ResponseEntity<List<GoerModel>> getGoersByPromoter(@PathVariable("idPromoter") String idPromoter) {
        List<GoerModel> result;
        result = goerService.getGoersByPromoter(idPromoter);
        if (result == null) {
            return new ResponseEntity<>(null, HttpStatus.SEE_OTHER);
        } else {
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }
	
}
