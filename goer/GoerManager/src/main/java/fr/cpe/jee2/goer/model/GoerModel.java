package fr.cpe.jee2.goer.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.cpe.jee2.event.dto.EventModelDTO;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.group.dto.GroupModelDTO;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "goer")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class GoerModel implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private String name;
	private String surname;
	private String mail;
	private String photo;
	private String banner;
	private String description;
	private Date birthDate;
	private String location;

	private transient List<GroupModelDTO> groupList = new ArrayList<>();
	private transient List<EventModelDTO> eventList = new ArrayList<>();
	private transient List<GoerModelDTO> friendList = new ArrayList<>();
	private transient List<PromoterModelDTO> promoterList = new ArrayList<>();

	public GoerModel(String name,String surname, String mail, String photo,String banner, String description, Date birthDate, String location) {
		this.name = name;
		this.surname= surname;
		this.mail = mail;
		this.photo = photo;
		this.banner=banner;
		this.description = description;
		this.birthDate = birthDate;
		this.location = location;
	}

	public GoerModel() {}

	@Override
	public String toString() {
		return "GoerModel{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", surname='" + surname + '\'' +
				", mail='" + mail + '\'' +
				", photo='" + photo + '\'' +
				", banner='" + banner + '\'' +
				", description='" + description + '\'' +
				", birthDate=" + birthDate +
				", location='" + location + '\'' +
				", groupList=" + groupList +
				", eventList=" + eventList +
				", friendList=" + friendList +
				", promoterList=" + promoterList +
				'}';
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhoto() {
		return photo;
	}

	public String getSurname() {return surname;}

	public void setSurname(String surname) {this.surname = surname;}

	public String getBanner() {return banner;}

	public void setBanner(String banner) {this.banner = banner;}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public List<GroupModelDTO> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<GroupModelDTO> groupList) {
		this.groupList = groupList;
	}

	public List<EventModelDTO> getEventList() {
		return eventList;
	}

	public void setEventList(List<EventModelDTO> eventList) {
		this.eventList = eventList;
	}

	public List<GoerModelDTO> getFriendList() {
		return friendList;
	}

	public void setFriendList(List<GoerModelDTO> friendList) {
		this.friendList = friendList;
	}

	public List<PromoterModelDTO> getPromoterList() {
		return promoterList;
	}

	public void setPromoterList(List<PromoterModelDTO> promoterList) {
		this.promoterList = promoterList;
	}
}
