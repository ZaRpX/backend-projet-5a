package fr.cpe.jee2.goer.service.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cpe.jee2.event.dto.EventModelDTO;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.goer.model.GoerModel;
import fr.cpe.jee2.goer.repository.GoerRepository;
import fr.cpe.jee2.goer.service.GoerService;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import fr.cpe.jee2.relation.RelationTypesEnum;
import fr.cpe.jee2.relation.dto.RelationModelDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.github.npathai.hamcrestopt.OptionalMatchers.hasValue;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.Silent.class)
public class GoerServiceTest {

    @Mock
    GoerRepository goerRepository;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    @Spy
    GoerService goerService;

    @Test
    public void createGoer() {
        GoerModel goerModel = new GoerModel();
        goerModel.setName("");
        goerModel.setMail("a");
        goerModel.setDescription("");

        assertEquals(goerService.createGoer(goerModel),"empty");

        goerModel.setName("test");
        assertEquals(goerService.createGoer(goerModel),"wrong mail");

        goerModel.setMail("aaa@aaa.fr");
        assertEquals(goerService.createGoer(goerModel),"empty");
    }

    @Test
    public void getAllGoers() {
        List<GoerModel> arrayList = new ArrayList<>();
        when(goerRepository.findAll()).thenReturn(arrayList);
        assertEquals(goerService.getAllGoers(),arrayList);
    }

    @Test
    public void getGoerById() {
        String id = "test";
        GoerModel eventModel = new GoerModel();
        when(goerRepository.findById(id)).thenReturn(Optional.of(eventModel));
        assertThat(goerService.getGoerById(id), hasValue(eventModel));
    }

    @Test
    public void getGoersFriends() {

        String idGoer = "5e28594b1c9d440000deca36";
        String retour = "[{\"id\":\"5e2860e31c9d440000deca3d\",\"idUser\":\"5e28594b1c9d440000deca36\",\"type\":\"friendship\",\"idEntity\":\"5e2858c61c9d440000deca35\",\"user\":null,\"entity\":null},{\"id\":\"5e2861761c9d440000deca40\",\"idUser\":\"5e2859b01c9d440000deca37\",\"type\":\"friendship\",\"idEntity\":\"5e28594b1c9d440000deca36\",\"user\":null,\"entity\":null}]";
        GoerModel goerModel = new GoerModel();

        Mockito.when(goerService.getGoerById(idGoer)).thenReturn(Optional.empty());
        Assert.assertNull(goerService.getGoersFriends(idGoer));

        Mockito.when(goerService.getGoerById(idGoer)).thenReturn(Optional.of(goerModel));
        Mockito.when(restTemplate.getForObject("https://relation-dot-festsharev2.appspot.com/relation/goer/" + idGoer + "/friendship", String.class)).thenReturn(retour);
        Mockito.when(goerRepository.findById(any())).thenReturn(Optional.of(goerModel));

        assertEquals(goerService.getGoersFriends(idGoer).size(), 2);
    }

    @Test
    public void removeGoer() {
        String id = "test";
        GoerModel groupModel = new GoerModel();
        Mockito.when(goerRepository.findById(id)).thenReturn(Optional.of(groupModel));

        goerService.removeGoer(id);
        Mockito.verify(goerRepository, Mockito.times(1)).delete(groupModel);
    }

    @Test
    public void getGoersByEvent() {
        String idEvent = "5e281737e33a1443c4fa5393";
        String retour = "[{\"id\":\"5e28594b1c9d440000deca36\",\"name\":\"Simon\",\"surname\":\"Drouin\",\"mail\":\"simon@festshare.com\",\"photo\":\"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkz7DoFsd3t5eC4IL_3BVkE-DI9O9TfTKlWHw-sUiuKH-6p-xY&s\",\"banner\":\"http://photos.sallesobscures.com/825801.jpg\",\"description\":\"Je m'appelle Simon, mais mes amis me surnomment \\\"Bob Spagett\\\" en raison de mes cheveux de couleur Or flamboyant. Si vous appr�ciez la musique commerciale, comme Aya Nakamura ou encore Callogero, envoyez-moi une demande d'ami pour que nous puissions faire connaissance et pourquoi pas nous rendre dans un festival ensemble pour les plus fous ;-) ;-) ;-)\",\"birthDate\":\"1998-10-08T22:00:00.000+0000\",\"location\":\"Bourg-en-Bresse\",\"groupList\":[],\"eventList\":[],\"friendList\":[],\"promoterList\":[]},{\"id\":\"5e285a2d1c9d440000deca38\",\"name\":\"Lo�c\",\"surname\":\"Ousset\",\"mail\":\"loic@festshare.com\",\"photo\":\"https://static.boredpanda.com/blog/wp-content/uploads/2013/12/tuna-melts-my-heart-raw.jpg\",\"banner\":\"https://media.playstation.com/is/image/SCEA/call-of-duty-modern-warfare-hero-banner-03-ps4-us-30may19?$native_nt$\",\"description\":\"Bonjour � tous, moi c'est Lo�c dit Lolo. Ce que j'aime dans la vie, c'est partager un bon moment en festival en �coutant de la musique au tempo �lev�. Je suis l�g�rement timide, mais n'h�sitez-pas � m'envoyer vos messages ! Je ne mords pas (???)\",\"birthDate\":\"1997-06-03T22:00:00.000+0000\",\"location\":\"Grenoble\",\"groupList\":[],\"eventList\":[],\"friendList\":[],\"promoterList\":[]}]";
        GoerModel goerModel = new GoerModel();
        EventModelDTO eventModelDTO = new EventModelDTO();


        Mockito.when(restTemplate.getForObject("https://event-dot-festsharev2.appspot.com/event/" + idEvent, EventModelDTO.class)).thenReturn(null);
        Assert.assertNull(goerService.getGoersByEvent(idEvent));

        Mockito.when(restTemplate.getForObject("https://event-dot-festsharev2.appspot.com/event/" + idEvent, EventModelDTO.class)).thenReturn(eventModelDTO);
        Mockito.when(restTemplate.getForObject("https://relation-dot-festsharev2.appspot.com/relation/entity/" + idEvent + "/followEvent", String.class)).thenReturn(retour);
        Mockito.when(goerRepository.findById(any())).thenReturn(Optional.of(goerModel));

        assertEquals(goerService.getGoersByEvent(idEvent).size(), 2);

    }

    @Test
    public void getGoersByPromoter() {

        String idPromoter = "5e281737e33a1443c4fa5393";
        String retour = "[{\"id\":\"5e28594b1c9d440000deca36\",\"name\":\"Simon\",\"surname\":\"Drouin\",\"mail\":\"simon@festshare.com\",\"photo\":\"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSkz7DoFsd3t5eC4IL_3BVkE-DI9O9TfTKlWHw-sUiuKH-6p-xY&s\",\"banner\":\"http://photos.sallesobscures.com/825801.jpg\",\"description\":\"Je m'appelle Simon, mais mes amis me surnomment \\\"Bob Spagett\\\" en raison de mes cheveux de couleur Or flamboyant. Si vous appr�ciez la musique commerciale, comme Aya Nakamura ou encore Callogero, envoyez-moi une demande d'ami pour que nous puissions faire connaissance et pourquoi pas nous rendre dans un festival ensemble pour les plus fous ;-) ;-) ;-)\",\"birthDate\":\"1998-10-08T22:00:00.000+0000\",\"location\":\"Bourg-en-Bresse\",\"groupList\":[],\"eventList\":[],\"friendList\":[],\"promoterList\":[]},{\"id\":\"5e285a2d1c9d440000deca38\",\"name\":\"Lo�c\",\"surname\":\"Ousset\",\"mail\":\"loic@festshare.com\",\"photo\":\"https://static.boredpanda.com/blog/wp-content/uploads/2013/12/tuna-melts-my-heart-raw.jpg\",\"banner\":\"https://media.playstation.com/is/image/SCEA/call-of-duty-modern-warfare-hero-banner-03-ps4-us-30may19?$native_nt$\",\"description\":\"Bonjour � tous, moi c'est Lo�c dit Lolo. Ce que j'aime dans la vie, c'est partager un bon moment en festival en �coutant de la musique au tempo �lev�. Je suis l�g�rement timide, mais n'h�sitez-pas � m'envoyer vos messages ! Je ne mords pas (???)\",\"birthDate\":\"1997-06-03T22:00:00.000+0000\",\"location\":\"Grenoble\",\"groupList\":[],\"eventList\":[],\"friendList\":[],\"promoterList\":[]}]";
        GoerModel goerModel = new GoerModel();
        PromoterModelDTO promoterModelDTO = new PromoterModelDTO();


        Mockito.when(restTemplate.getForObject("https://promoter-dot-festsharev2.appspot.com/promoter/" + idPromoter, PromoterModelDTO.class)).thenReturn(null);
        Assert.assertNull(goerService.getGoersByPromoter(idPromoter));

        Mockito.when(restTemplate.getForObject("https://promoter-dot-festsharev2.appspot.com/promoter/" + idPromoter, PromoterModelDTO.class)).thenReturn(promoterModelDTO);
        Mockito.when(restTemplate.getForObject("https://relation-dot-festsharev2.appspot.com/relation/entity/" + idPromoter + "/followPromoter", String.class)).thenReturn(retour);
        Mockito.when(goerRepository.findById(any())).thenReturn(Optional.of(goerModel));

        assertEquals(goerService.getGoersByPromoter(idPromoter).size(), 2);
    }
}

