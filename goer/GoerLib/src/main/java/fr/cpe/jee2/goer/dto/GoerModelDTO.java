package fr.cpe.jee2.goer.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GoerModelDTO implements Serializable {

    private String id;
    private String name;
    private String surname;
    private String mail;
    private String photo;
    private String banner;
    private String description;
    private Date birthDate;
    private String location;

    public GoerModelDTO(String id, String name, String surname, String mail, String photo, String banner, String description, Date birthDate, String location) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.mail = mail;
        this.photo = photo;
        this.banner = banner;
        this.description = description;
        this.birthDate = birthDate;
        this.location = location;
    }

    public GoerModelDTO() {}

    @Override
    public String toString() {
        return "GoerModelDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", mail='" + mail + '\'' +
                ", photo='" + photo + '\'' +
                ", banner='" + banner + '\'' +
                ", description='" + description + '\'' +
                ", birthDate=" + birthDate +
                ", location='" + location + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {return surname;}

    public void setSurname(String surname) {this.surname = surname;}

    public String getBanner() {return banner;}

    public void setBanner(String banner) {this.banner = banner;}

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
