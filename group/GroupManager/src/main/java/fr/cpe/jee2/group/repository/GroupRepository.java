package fr.cpe.jee2.group.repository;

import fr.cpe.jee2.group.model.GroupModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends MongoRepository<GroupModel, String> {

    List<GroupModel> findByIdEvent(String idEvent);

}
