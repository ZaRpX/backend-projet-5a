package fr.cpe.jee2.group.controller;

import fr.cpe.jee2.group.model.GroupModel;
import fr.cpe.jee2.group.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class GroupRestController {

    @Autowired
    private GroupService groupService;

    @RequestMapping(method=RequestMethod.POST,value="/group")
    public ResponseEntity<String> postGroup(@RequestBody GroupModel group) {
        String result = groupService.createGroup(group);

        switch(result){
            case "error":
                return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
            case "empty":
                return new ResponseEntity<>("missing parameter", HttpStatus.NO_CONTENT);
            case "not found":
                return new ResponseEntity<>("event not found", HttpStatus.SEE_OTHER);
            default:
                return new ResponseEntity<>(group.toString(), HttpStatus.CREATED);
        }
    }

    @RequestMapping("/groups")
    private ResponseEntity<List<GroupModel>> getAllGroups() {
        return new ResponseEntity<>(groupService.getAllGroups(), HttpStatus.OK);
    }

    @RequestMapping("/group/{id}")
    private ResponseEntity<GroupModel> getGroupById(@PathVariable String id) {
        Optional<GroupModel> group;
        group = groupService.getGroupById(id);
        return new ResponseEntity<>(group.orElse(null), HttpStatus.OK);
    }

    @RequestMapping("/group/event/{idEvent}")
    private ResponseEntity<List<GroupModel>> getGroupByEvent(@PathVariable String idEvent) {
        List<GroupModel> groups;
        groups = groupService.getGroupByEvent(idEvent);
        if((groups == null)||(groups.isEmpty()))
            return new ResponseEntity<>(null, HttpStatus.SEE_OTHER);
        else
            return new ResponseEntity<>(groups, HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/group/{id}")
    public ResponseEntity<String> deleteGroup(@PathVariable String id){
        String result = groupService.removeGroup(id);
        if(result.equals("error"))
            return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<>(id + "deleted", HttpStatus.OK);
    }
	
}
