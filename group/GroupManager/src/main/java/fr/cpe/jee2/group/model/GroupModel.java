package fr.cpe.jee2.group.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.cpe.jee2.event.dto.EventModelDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "group")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class GroupModel implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private String name;
	private String idEvent;

	private transient EventModelDTO event;

	public GroupModel(String name, String idEvent) {
		this.name = name;
		this.idEvent = idEvent;
	}

	public GroupModel() {}

	@Override
	public String toString() {
		return "GroupModel{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", idEvent='" + idEvent + '\'' +
				", event=" + event +
				'}';
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdEvent() {
		return idEvent;
	}

	public void setIdEvent(String idEvent) {
		this.idEvent = idEvent;
	}

	public EventModelDTO getEvent() {
		return event;
	}

	public void setEvent(EventModelDTO event) {
		this.event = event;
	}
}
