package fr.cpe.jee2.group.service;

import fr.cpe.jee2.event.dto.EventModelDTO;
import fr.cpe.jee2.group.model.GroupModel;
import fr.cpe.jee2.group.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class GroupService {

    @Autowired
    private GroupRepository groupRepository;

    RestTemplate restTemplate = new RestTemplate();

    public String createGroup( GroupModel group) {

        //Contrôle d'existence de l'event
        String eventUrl = "https://event-dot-festsharev2.appspot.com/event/" + group.getIdEvent();

        EventModelDTO responseEvent = restTemplate.getForObject(
                eventUrl,
                EventModelDTO.class);

        if (responseEvent==null)
            return "not found";

        //Contrôle de nom non vide
        if (group.getName().isEmpty())
            return "empty";

        try {
            groupRepository.save(group);
            return group.toString();
        } catch (Exception e) {
            return "error";
        }
    }

    public List<GroupModel> getAllGroups(){ return groupRepository.findAll();}

    public Optional<GroupModel> getGroupById(String id) {
        return groupRepository.findById(id);
    }


    public List<GroupModel> getGroupByEvent(String idEvent) {

        List<GroupModel> resultList = new ArrayList<>();

        //Contrôle d'existence de l'event
        String eventUrl = "https://event-dot-festsharev2.appspot.com/event/" + idEvent;

        EventModelDTO responseEvent = restTemplate.getForObject(
                eventUrl,
                EventModelDTO.class);

        if (responseEvent==null)
            return null;

        resultList.addAll(groupRepository.findByIdEvent(idEvent));

        return resultList;
    }

    public String removeGroup(String id) {
        try {
            Optional<GroupModel> groupModel = groupRepository.findById(id);
            groupRepository.delete(groupModel.get());
            return id;
        } catch (Exception e) {
            return "error";
        }
    }
}

