package fr.cpe.jee2.group.service.test;

import com.github.npathai.hamcrestopt.OptionalMatchers;
import fr.cpe.jee2.event.dto.EventModelDTO;
import fr.cpe.jee2.group.model.GroupModel;
import fr.cpe.jee2.group.repository.GroupRepository;
import fr.cpe.jee2.group.service.GroupService;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GroupServiceTest {

    @Mock
    GroupRepository groupRepository;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    @Spy
    GroupService groupServiceService;

    @Test
    public void createGroup(){
        //Grace � ce test, on a d�couvert que la fonction createGroup avait un probleme de s�curit� si le nom est null
        //Il est volontairement laiss� ici pour montrer l'utilit� des tests

        GroupModel groupModel = new GroupModel();
        EventModelDTO eventModelDTO = new EventModelDTO();
        groupModel.setIdEvent("test");

        //On test dans le cas o� l'event n'existe pas
        Mockito.when(restTemplate.getForObject("https://event-dot-festsharev2.appspot.com/event/" + groupModel.getIdEvent(), EventModelDTO.class)).thenReturn(null);
        Assert.assertEquals(groupServiceService.createGroup(groupModel),"not found");

        //On test dans le cas o� le nom du groupe est vide
        Mockito.when(restTemplate.getForObject("https://event-dot-festsharev2.appspot.com/event/" + groupModel.getIdEvent(), EventModelDTO.class)).thenReturn(eventModelDTO);
        // Voir commentaire au dessus
        Assert.assertEquals(groupServiceService.createGroup(groupModel),"empty");

        //Sinon on verifie que l'on retourne bien le groupe en format string
        groupModel.setName("test");
        Mockito.when(groupRepository.save(groupModel)).thenReturn(null);
        Assert.assertThat(groupServiceService.createGroup(groupModel), CoreMatchers.instanceOf(String.class));
    }

    @Test
    public void getAllGroups(){
        //Aucune logique m�tier, on verifie juste la bonne execution
        List<GroupModel> arrayList = new ArrayList<>();
        Mockito.when(groupRepository.findAll()).thenReturn(arrayList);
        Assert.assertEquals(groupServiceService.getAllGroups(),arrayList);
    }

    @Test
    public void getGroupById() {
        String id = "test";
        GroupModel groupModel = new GroupModel();
        Mockito.when(groupRepository.findById(id)).thenReturn(Optional.of(groupModel));
        Assert.assertThat(groupServiceService.getGroupById(id), OptionalMatchers.hasValue(groupModel));
    }

    @Test
    public void getGroupByEvent() {

        String idEvent = "test";

        //On test le cas ou l'�v�nement n'est pas pr�sent
        Mockito.when(restTemplate.getForObject("https://event-dot-festsharev2.appspot.com/event/" + idEvent,
                EventModelDTO.class)).thenReturn(null);
        Assert.assertNull(groupServiceService.getGroupByEvent(idEvent));

        //On v�rifie que l'on retourne bien une liste de groupe
        Mockito.when(restTemplate.getForObject("https://event-dot-festsharev2.appspot.com/event/" + idEvent,
                EventModelDTO.class)).thenReturn(new EventModelDTO());
        Mockito.when(groupRepository.findByIdEvent(idEvent)).thenReturn(new ArrayList<>());
        Assert.assertThat(groupServiceService.getGroupByEvent(idEvent), CoreMatchers.instanceOf(ArrayList.class));
    }

    @Test
    public void removeGroup() {
        String id = "test";
        GroupModel groupModel = new GroupModel();
        Mockito.when(groupRepository.findById(id)).thenReturn(Optional.of(groupModel));

        groupServiceService.removeGroup(id);
        Mockito.verify(groupRepository, Mockito.times(1)).delete(groupModel);
    }
}