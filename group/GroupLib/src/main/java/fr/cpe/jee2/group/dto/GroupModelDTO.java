package fr.cpe.jee2.group.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupModelDTO implements Serializable {

    private String id;
    private String name;
    private String idEvent;

    public GroupModelDTO(String name, String idEvent) {
        this.name = name;
        this.idEvent = idEvent;
    }

    public GroupModelDTO() {}

    @Override
    public String toString() {
        return "GroupModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", idEvent='" + idEvent + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(String idEvent) {
        this.idEvent = idEvent;
    }
}
