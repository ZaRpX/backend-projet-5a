package fr.cpe.jee2.login.controller;

import fr.cpe.jee2.login.model.LoginModel;
import fr.cpe.jee2.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class LoginRestController {

    @Autowired
    private LoginService loginService;

    @RequestMapping(method=RequestMethod.POST,value="/register")
    public ResponseEntity<String> register(@RequestBody LoginModel login) {
        String result = loginService.createLogin(login);

        switch (result){
            case "error":
                return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
            case "alreadyExists":
                return new ResponseEntity<>("username already exists", HttpStatus.NOT_ACCEPTABLE);
            default:
                return new ResponseEntity<>(login.toString(), HttpStatus.CREATED);
        }
    }

    @RequestMapping("/login/{id}")
    private ResponseEntity<LoginModel> getLoginById(@PathVariable String id) {
        Optional<LoginModel> login;
        login = loginService.getLoginById(id);
        return new ResponseEntity<>(login.orElse(null), HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/login/{id}")
    public ResponseEntity<String> deleteLogin(@PathVariable String id){
        String result = loginService.removeLogin(id);
        if(result.equals("error"))
            return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<>(id + "deleted", HttpStatus.OK);
    }
}
