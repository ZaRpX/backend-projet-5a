package fr.cpe.jee2.login.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import fr.cpe.jee2.login.LoginTypeEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "login")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class LoginModel implements Serializable {

    @Id
    private String id;

    private String username;
    private String password;
    private String idUser;
    private LoginTypeEnum type;

    private transient GoerModelDTO goer;
    private transient PromoterModelDTO promoter;

    public LoginModel(String username, String password, String idUser, LoginTypeEnum type){
        this.username = username;
        this.password = password;
        this.idUser = idUser;
        this.type = type;
    }

    public LoginModel() {}

    @Override
    public String toString() {
        return "LoginModel{" +
                "id=" + id +
                ", username=" + username +
                ", password='" + password + '\'' +
                '}';
    }

    public String getId() {return id;}

    public void setId(String id) {this.id = id;}

    public String getUsername() {return username;}

    public void setUsername(String username) {this.username = username;}

    public String getPassword() {return password;}

    public void setPassword(String password) {this.password = password;}

    public String getIdUser() {return idUser;}

    public void setIdUser(String idUser) {this.idUser = idUser;}

    public LoginTypeEnum getType() {return type;}

    public void setType(LoginTypeEnum type) {this.type = type;}
}
