package fr.cpe.jee2.login.service;
import fr.cpe.jee2.login.model.LoginModel;
import fr.cpe.jee2.login.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class LoginService {

    @Autowired
    private LoginRepository loginRepository;

    public String createLogin( LoginModel login) {

        //RestTemplate restTemplate = new RestTemplate();

        LoginModel exist = loginRepository.findByUsername(login.getUsername());

        if (exist!=null)
            return "alreadyExists";

        try {
            loginRepository.save(login);
            /*if (login.getType().equals(LoginTypeEnum.promoter)){

                String promoterUrl = "https://promoter-dot-festsharev2.appspot.com/promoter";

            }*/
            return login.toString();
        } catch (Exception e) {
            return "error";
        }
    }

    public Optional<LoginModel> getLoginById(String id){
        return loginRepository.findById(id);
    }

    public String removeLogin(String id) {
        try {
            Optional<LoginModel> loginModel = loginRepository.findById(id);
            loginRepository.delete(loginModel.get());
            return id;
        } catch (Exception e) {
            return "error";
        }
    }
}

