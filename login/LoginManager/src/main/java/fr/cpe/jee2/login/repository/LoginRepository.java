package fr.cpe.jee2.login.repository;

import fr.cpe.jee2.login.model.LoginModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends MongoRepository<LoginModel, String> {

    LoginModel findByUsername(String username);

}
