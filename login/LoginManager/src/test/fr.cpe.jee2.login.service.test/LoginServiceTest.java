package fr.cpe.jee2.login.service.test;

import fr.cpe.jee2.login.model.LoginModel;
import fr.cpe.jee2.login.repository.LoginRepository;
import fr.cpe.jee2.login.service.LoginService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LoginServiceTest {

    @Mock
    LoginRepository loginRepository;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    @Spy
    LoginService loginService;

    @Test
    public void createLogin() {
        LoginModel loginModel = new LoginModel();
        loginModel.setUsername("test");

        //On test la verification de l'existance d'un login
        Mockito.when(loginRepository.findByUsername(loginModel.getUsername())).thenReturn(new LoginModel());
        Assert.assertEquals(loginService.createLogin(loginModel), "alreadyExists");

        //On test si l'objet retourne et bien le login
        Mockito.when(loginRepository.findByUsername(loginModel.getUsername())).thenReturn(null);
        Mockito.when(loginRepository.save(loginModel)).thenReturn(null);
        Assert.assertEquals(loginService.createLogin(loginModel), loginModel.toString());
    }

    @Test
    public void getLoginById(){
        String id = "test";
        LoginModel loginModel = new LoginModel();
        loginModel.setId(id);
        Optional<LoginModel> optionalLogin = Optional.of(loginModel);

        //Pas très important car l'on vérifie que l'on retourne ce que l'on mock
        Mockito.when(loginRepository.findById(id)).thenReturn(optionalLogin);
        Assert.assertEquals(loginService.getLoginById(id),optionalLogin);
    }

    @Test
    public void removeLogin(){
        String id = "test";
        LoginModel loginModel = new LoginModel();
        loginModel.setId(id);
        Optional<LoginModel> optionalLogin = Optional.of(loginModel);

        //On mock les méthodes de recherche/suppression
        Mockito.when(loginRepository.findById(id)).thenReturn(optionalLogin);
        Mockito.doNothing().when(loginRepository).delete(loginModel);
        Assert.assertEquals(loginService.removeLogin(id), id);
    }
}
