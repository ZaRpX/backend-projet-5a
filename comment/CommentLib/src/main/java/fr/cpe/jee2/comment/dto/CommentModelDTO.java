package fr.cpe.jee2.comment.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.cpe.jee2.comment.CommentTypesEnum;
import fr.cpe.jee2.login.LoginTypeEnum;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentModelDTO implements Serializable {

    //@GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    //id de l'auteur du message/commentaire
    private String idUser;
    //type du topic auquel le message est attaché
    private CommentTypesEnum type;
    //id du topic auquel le message est attaché
    private String idTopic;
    //Contenu textuel du message/commentaire
    private String content;

    private Date date;

    private LoginTypeEnum role;

    //Objet non enregistré en base - lien vers l'event/group/post
    private transient Object topic;

    public CommentModelDTO(String idUser, CommentTypesEnum type, String idTopic, String content, Date date, LoginTypeEnum role) {
        this.idUser = idUser;
        this.type = type;
        this.idTopic = idTopic;
        this.content = content;
        this.date = date;
        this.role = role;
    }

    public CommentModelDTO() {}

    @Override
    public String toString() {
        return "CommentModel{" +
                "id='" + id + '\'' +
                ", idUser='" + idUser + '\'' +
                ", type=" + type +
                ", idTopic='" + idTopic + '\'' +
                ", content='" + content + '\'' +
                ", role=" + role +
                ", topic=" + topic +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdTopic() {
        return idTopic;
    }

    public void setIdTopic(String idTopic) {
        this.idTopic = idTopic;
    }

    public String getIdUser() {return idUser;}

    public void setIdUser(String idUser) {this.idUser = idUser;}

    public String getContent() {return content;}

    public void setContent(String content) {this.content = content;}

    public CommentTypesEnum getType() {
        return type;
    }

    public void setType(CommentTypesEnum type) {
        this.type = type;
    }

    public Object getTopic() {
        return topic;
    }

    public void setTopic(Object topic) {
        this.topic = topic;
    }

    public LoginTypeEnum getRole() {
        return role;
    }

    public void setRole(LoginTypeEnum role) {
        this.role = role;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
