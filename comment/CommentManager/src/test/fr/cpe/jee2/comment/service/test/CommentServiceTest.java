package fr.cpe.jee2.comment.service.test;

import fr.cpe.jee2.comment.CommentTypesEnum;
import fr.cpe.jee2.comment.model.CommentModel;
import fr.cpe.jee2.comment.repository.CommentRepository;
import fr.cpe.jee2.comment.service.CommentService;
import fr.cpe.jee2.event.dto.EventModelDTO;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.group.dto.GroupModelDTO;
import fr.cpe.jee2.login.LoginTypeEnum;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static com.github.npathai.hamcrestopt.OptionalMatchers.hasValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CommentServiceTest {

    @Mock
    CommentRepository commentRepository;

    @Spy
    RestTemplate restTemplate;

    @InjectMocks
    CommentService commentService;

    @Test
    public void createComment() {
        CommentModel commentModel = new CommentModel();

        commentModel.setRole(LoginTypeEnum.promoter);
        commentModel.setIdUser("test");
        when(restTemplate.getForObject("https://promoter-dot-festsharev2.appspot.com/promoter/" + commentModel.getIdUser(), PromoterModelDTO.class)).thenReturn(null);
        assertEquals(commentService.createComment(commentModel),"not found");

        commentModel.setRole(LoginTypeEnum.goer);
        commentModel.setIdUser("test");
        when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + commentModel.getIdUser(), GoerModelDTO.class)).thenReturn(null);
        assertEquals(commentService.createComment(commentModel),"not found");

        commentModel.setRole(LoginTypeEnum.goer);
        commentModel.setIdUser("test");
        commentModel.setType(CommentTypesEnum.adminMessage);
        when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + commentModel.getIdUser(), GoerModelDTO.class)).thenReturn(null);
        assertEquals(commentService.createComment(commentModel),"not admin");

        commentModel.setRole(LoginTypeEnum.goer);
        commentModel.setIdUser("test");
        commentModel.setType(CommentTypesEnum.eventComment);
        commentModel.setIdTopic("test");
        when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + commentModel.getIdUser(), GoerModelDTO.class)).thenReturn(new GoerModelDTO());
        when(restTemplate.getForObject("https://event-dot-festsharev2.appspot.com/event/" + commentModel.getIdTopic(), EventModelDTO.class)).thenReturn(null);
        assertEquals(commentService.createComment(commentModel),"not found");

        commentModel.setRole(LoginTypeEnum.goer);
        commentModel.setIdUser("test");
        commentModel.setType(CommentTypesEnum.groupComment);
        commentModel.setIdTopic("test");
        when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + commentModel.getIdUser(), GoerModelDTO.class)).thenReturn(new GoerModelDTO());
        when(restTemplate.getForObject("https://group-dot-festsharev2.appspot.com/group/" + commentModel.getIdTopic(), GroupModelDTO.class)).thenReturn(null);
        assertEquals(commentService.createComment(commentModel),"not found");
    }

    @Test
    public void getCommentById() {
        String id = "test";
        CommentModel commentModel = new CommentModel();
        when(commentRepository.findById(id)).thenReturn(Optional.of(commentModel));
        assertThat(commentService.getCommentById(id),hasValue(commentModel));
    }

    @Test
    public void removeComment() {
        String id = "test";
        CommentModel commentModel = new CommentModel();
        when(commentRepository.findById(id)).thenReturn(Optional.of(commentModel));

        commentService.removeComment(id);
        verify(commentRepository, times(1)).delete(commentModel);
    }
}