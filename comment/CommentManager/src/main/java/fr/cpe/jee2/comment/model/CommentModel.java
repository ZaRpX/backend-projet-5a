package fr.cpe.jee2.comment.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.cpe.jee2.comment.CommentTypesEnum;
import fr.cpe.jee2.login.LoginTypeEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Document(collection = "comment")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class CommentModel implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	//id de l'auteur du message/commentaire
	private String idUser;
	//type du topic auquel le message est attaché
	private CommentTypesEnum type;
	//id du topic auquel le message est attaché
	private String idTopic;
	//Contenu textuel du message/commentaire
	private String content;

	private Date date;

	private LoginTypeEnum role;

	//Objet non enregistré en base - lien vers l'event/group/post
	private transient Object topic;

	public CommentModel(String idUser, CommentTypesEnum type, String idTopic, String content, Date date, LoginTypeEnum role) {
		this.idUser = idUser;
		this.type = type;
		this.idTopic = idTopic;
		this.content = content;
		this.date = date;
		this.role = role;
	}

	public CommentModel() {}

	@Override
	public String toString() {
		return "CommentModel{" +
				"id='" + id + '\'' +
				", idUser='" + idUser + '\'' +
				", type=" + type +
				", idTopic='" + idTopic + '\'' +
				", content='" + content + '\'' +
				", role=" + role +
				", topic=" + topic +
				'}';
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdTopic() {
		return idTopic;
	}

	public void setIdTopic(String idTopic) {
		this.idTopic = idTopic;
	}

	public String getIdUser() {return idUser;}

	public void setIdUser(String idUser) {this.idUser = idUser;}

	public String getContent() {return content;}

	public void setContent(String content) {this.content = content;}

	public CommentTypesEnum getType() {
		return type;
	}

	public void setType(CommentTypesEnum type) {
		this.type = type;
	}

	public Object getTopic() {
		return topic;
	}

	public void setTopic(Object topic) {
		this.topic = topic;
	}

	public LoginTypeEnum getRole() {
		return role;
	}

	public void setRole(LoginTypeEnum role) {
		this.role = role;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
