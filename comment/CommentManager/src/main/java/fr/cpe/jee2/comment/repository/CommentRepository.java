package fr.cpe.jee2.comment.repository;

import fr.cpe.jee2.comment.model.CommentModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends MongoRepository<CommentModel, String> {

}
