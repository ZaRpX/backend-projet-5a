package fr.cpe.jee2.comment.controller;

import fr.cpe.jee2.comment.model.CommentModel;
import fr.cpe.jee2.comment.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class CommentRestController {

    @Autowired
    private CommentService commentService;

    @RequestMapping(method=RequestMethod.POST,value="/comment")
    public ResponseEntity<String> postComment(@RequestBody CommentModel comment) {
        String result = commentService.createComment(comment);
        switch (result) {
            case "error":
                return new ResponseEntity<>("error", HttpStatus.BAD_REQUEST);
            case "not found":
                return new ResponseEntity<>("not found", HttpStatus.SEE_OTHER);
            case "not admin":
                return new ResponseEntity<>("not admin", HttpStatus.FORBIDDEN);
            case "empty":
                return new ResponseEntity<>("empty", HttpStatus.NO_CONTENT);
            default:
                return new ResponseEntity<>(comment.toString(), HttpStatus.CREATED);
        }
    }

        @RequestMapping("/comment/{id}")
    private ResponseEntity<CommentModel> getCommentById(@PathVariable String id) {
        Optional<CommentModel> comment;
        comment = commentService.getCommentById(id);
        return new ResponseEntity<>(comment.orElse(null), HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/comment/{id}")
    public ResponseEntity<String> deleteComment(@PathVariable String id){
        String result = commentService.removeComment(id);
        if(result.equals("error"))
            return new ResponseEntity<>("error", HttpStatus.BAD_REQUEST);
        else
            return new ResponseEntity<>(id + "deleted", HttpStatus.OK);
    }

}
