package fr.cpe.jee2.comment.service;
import fr.cpe.jee2.comment.CommentTypesEnum;
import fr.cpe.jee2.comment.model.CommentModel;
import fr.cpe.jee2.comment.repository.CommentRepository;
import fr.cpe.jee2.event.dto.EventModelDTO;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.group.dto.GroupModelDTO;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import fr.cpe.jee2.login.LoginTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;


@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    public String createComment( CommentModel comment) {

        RestTemplate restTemplate = new RestTemplate();

        //Contrôle d'existence du user et du niveau de droits (goer or promoter)
        if(comment.getRole()== LoginTypeEnum.promoter){
            String promoterUrl = "https://promoter-dot-festsharev2.appspot.com/promoter/" + comment.getIdUser();

            PromoterModelDTO responsePromoter = restTemplate.getForObject(
                    promoterUrl,
                    PromoterModelDTO.class);

            if (responsePromoter==null)
                return "not found";

        } else if (comment.getRole()== LoginTypeEnum.goer){
            String goerUrl = "https://goer-dot-festsharev2.appspot.com/goer/" + comment.getIdUser();

            GoerModelDTO responseGoer = restTemplate.getForObject(
                    goerUrl,
                    GoerModelDTO.class);

            if(comment.getType()== CommentTypesEnum.adminMessage)
                return "not admin";
            if (responseGoer==null)
                return "not found";
        }

        //Contrôle d'existence du topic si c'est un event
        if ((comment.getType()==CommentTypesEnum.eventComment)||(comment.getType()==CommentTypesEnum.adminMessage)){
            String eventUrl = "https://event-dot-festsharev2.appspot.com/event/" + comment.getIdTopic();

            EventModelDTO responseEvent = restTemplate.getForObject(
                    eventUrl,
                    EventModelDTO.class);

            if (responseEvent==null)
                return "not found";
        }

        //Contrôle d'existence du topic si c'est un group
        if (comment.getType()== CommentTypesEnum.groupComment){
            String groupUrl = "https://group-dot-festsharev2.appspot.com/group/" + comment.getIdTopic();

            GroupModelDTO responseGroup = restTemplate.getForObject(
                    groupUrl,
                    GroupModelDTO.class);

            if (responseGroup==null)
                return "not found";
        }

        //Contrôle de contenu non vide
        if (comment.getContent().isEmpty())
            return "empty";

        try {
            commentRepository.save(comment);
            return comment.toString();
        } catch (Exception e) {
            return "error";
        }
    }

    public Optional<CommentModel> getCommentById(String id){
        return commentRepository.findById(id);
    }

    public String removeComment(String id) {
        try {
            Optional<CommentModel> commentModel = commentRepository.findById(id);
            commentRepository.delete(commentModel.get());
            return id;
        } catch (Exception e) {
            return "error";
        }
    }
}

