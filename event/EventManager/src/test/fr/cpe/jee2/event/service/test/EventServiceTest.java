package fr.cpe.jee2.event.service.test;

import fr.cpe.jee2.event.model.EventModel;
import fr.cpe.jee2.event.repository.EventRepository;
import fr.cpe.jee2.event.service.EventService;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.github.npathai.hamcrestopt.OptionalMatchers.hasValue;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EventServiceTest {

    @Mock
    EventRepository eventRepository;

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    @Spy
    EventService eventService;

    @Test
    public void createEvent() {
        EventModel eventModel = new EventModel();
        eventModel.setIdPromoter("test");
        eventModel.setDescription("test");
        eventModel.setName("test");

        when(eventRepository.findByDateAndPlaceAndNameAndIdPromoterAndStyle(eventModel.getDate(), eventModel.getPlace(), eventModel.getName(), eventModel.getIdPromoter(), eventModel.getStyle())).thenReturn(null);
        assertEquals(eventService.createEvent(eventModel),"alreadyExists");

        when(eventRepository.findByDateAndPlaceAndNameAndIdPromoterAndStyle(eventModel.getDate(), eventModel.getPlace(), eventModel.getName(), eventModel.getIdPromoter(), eventModel.getStyle())).thenReturn(new ArrayList<>());
        when(restTemplate.getForObject("https://promoter-dot-festsharev2.appspot.com/promoter/" + eventModel.getIdPromoter(), PromoterModelDTO.class)).thenReturn(null);
        assertEquals(eventService.createEvent(eventModel),"no promoter");
    }

    @Test
    public void updateEvent() {
        EventModel event = new EventModel();
        when(eventRepository.findById(event.getId())).thenReturn(Optional.empty());
        assertEquals(eventService.updateEvent((event)),"not found");

        EventModel newEvent = new EventModel();
        newEvent.setId("test");
        when(eventRepository.findById(newEvent.getId())).thenReturn(Optional.of(newEvent));
        eventService.updateEvent(newEvent);
        verify(eventRepository, times(1)).save(newEvent);
    }

    @Test
    public void getEvent() {
        String id = "test";
        EventModel eventModel = new EventModel();
        when(eventRepository.findById(id)).thenReturn(Optional.of(eventModel));
        assertThat(eventService.getEvent(id),hasValue(eventModel));
    }

    @Test
    public void getAllEvents() {
        //Aucune logique m�tier, on verifie juste la bonne execution
        List<EventModel> arrayList = new ArrayList<>();
        when(eventRepository.findAll()).thenReturn(arrayList);
        assertEquals(eventService.getAllEvents(),arrayList);
    }

    @Test
    public void getEventsWithParam() {
        //Aucune logique m�tier, on verifie juste la bonne execution
        String event = "test";
        List<EventModel> arrayList = new ArrayList<>();
        when(eventRepository.findByNameContainingOrDescriptionContaining(event, event)).thenReturn(arrayList);
        assertEquals(eventService.getEventsWithParam(event),arrayList);
    }

    @Test
    public void getEventsByPromoter() {
        //Aucune logique m�tier, on verifie juste la bonne execution
        String idPromoter = "test";
        List<EventModel> arrayList = new ArrayList<>();
        when(eventRepository.findByIdPromoter(idPromoter)).thenReturn(arrayList);
        assertEquals(eventService.getEventsByPromoter(idPromoter),arrayList);
    }

    @Test
    public void getEventsByGoer() {
        String idGoer = "5e28594b1c9d440000deca36";
        String retourRelation = "[{\"id\":\"5e2863611c9d440000deca45\",\"idUser\":\"5e28594b1c9d440000deca36\",\"type\":\"followEvent\",\"idEntity\":\"5e281737e33a1443c4fa5393\",\"user\":null,\"entity\":null},{\"id\":\"5e2863721c9d440000deca46\",\"idUser\":\"5e28594b1c9d440000deca36\",\"type\":\"followEvent\",\"idEntity\":\"5e281efce33a140e105d8fce\",\"user\":null,\"entity\":null}]";
        Optional<EventModel> e = Optional.of(new EventModel());

        //On test dans le cas o� le goer n'existe pas
        Mockito.when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + idGoer, GoerModelDTO.class)).thenReturn(null);
        assertNull(eventService.getEventsByGoer(idGoer));

        //On test dans le cas o� le goer n'existe pas
        Mockito.when(restTemplate.getForObject("https://goer-dot-festsharev2.appspot.com/goer/" + idGoer, GoerModelDTO.class)).thenReturn(new GoerModelDTO());
        Mockito.when(restTemplate.getForObject("https://relation-dot-festsharev2.appspot.com/relation/goer/" + idGoer + "/followEvent", String.class)).thenReturn(retourRelation);
        Mockito.when(eventRepository.findById(any())).thenReturn(e);
        Assert.assertThat(eventService.getEventsByGoer(idGoer), CoreMatchers.instanceOf(List.class));
    }

    @Test
    public void removeEvent() {
        String id = "test";
        EventModel eventModel = new EventModel();
        when(eventRepository.findById(id)).thenReturn(Optional.of(eventModel));

        eventService.removeEvent(id);
        verify(eventRepository, times(1)).delete(eventModel);
    }
}