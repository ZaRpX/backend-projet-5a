package fr.cpe.jee2.event.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cpe.jee2.event.model.EventModel;
import fr.cpe.jee2.event.repository.EventRepository;
import fr.cpe.jee2.goer.dto.GoerModelDTO;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import fr.cpe.jee2.relation.dto.RelationModelDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Service
public class EventService {

    @Autowired
    private EventRepository eventRepository;

    RestTemplate restTemplate = new RestTemplate();

    public String createEvent(EventModel event) {

        //Contrôle que l'event n'existe pas déjà
        if(eventRepository.findByDateAndPlaceAndNameAndIdPromoterAndStyle(event.getDate(), event.getPlace(), event.getName(), event.getIdPromoter(), event.getStyle())==null)
            return "alreadyExists";

        //Contrôle de l'existence du promoter
        String promoterUrl = "https://promoter-dot-festsharev2.appspot.com/promoter/" + event.getIdPromoter();

        PromoterModelDTO responsePromoter = restTemplate.getForObject(
                promoterUrl,
                PromoterModelDTO.class);

        if (responsePromoter==null)
            return "no promoter";

            //Contrôle de description non vide
        if (event.getDescription().isEmpty())
            return "empty";

        //Contrôle de nom non vide
        if (event.getName().isEmpty())
            return "empty";

        try {
            eventRepository.save(event);
            return event.toString();
        } catch (Exception e) {
            return "error";
        }
    }

    public String updateEvent(EventModel event) {

        Optional<EventModel> existingEvent = eventRepository.findById(event.getId());

        if(!existingEvent.isPresent())
            return "not found";

        if(event.getName() != null){
            existingEvent.get().setName(event.getName());
        }

        if(event.getDate() != null){
            existingEvent.get().setDate(event.getDate());
        }

        if(event.getDateFin() != null){
            existingEvent.get().setDateFin(event.getDateFin());
        }

        if(event.getPlace() != null){
            existingEvent.get().setPlace(event.getPlace());
        }

        if(event.getDescription() != null){
            existingEvent.get().setDescription(event.getDescription());
        }

        if(event.getIdPromoter() != null){
            existingEvent.get().setIdPromoter(event.getIdPromoter());
        }

        if(event.getStyle() != null){
            existingEvent.get().setStyle(event.getStyle());
        }

        if(event.getBannerUrl() != null){
            existingEvent.get().setBannerUrl(event.getBannerUrl());
        }

        try {
            eventRepository.save(existingEvent.get());
            return event.toString();
        } catch (Exception e) {
            return "error";
        }
    }

    public Optional<EventModel> getEvent(String id) {
        return eventRepository.findById(id);
    }

    public List<EventModel> getAllEvents() {
        return new ArrayList<>(eventRepository.findAll());
    }

    public List<EventModel> getEventsWithParam(String event) {
        return new ArrayList<>(eventRepository.findByNameContainingOrDescriptionContaining(event,event));
    }

    public List<EventModel> getEventsByPromoter(String idPromoter) {
        return new ArrayList<>(eventRepository.findByIdPromoter(idPromoter));
    }

    //Revoir la façon d'appeler un service Rest
    public List<EventModel> getEventsByGoer(String idGoer) {
        ObjectMapper mapper = new ObjectMapper();
        List<EventModel> resultList = new ArrayList<>();

        //Contrôle de l'existence du goer
        String goerUrl = "https://goer-dot-festsharev2.appspot.com/goer/" + idGoer;

        GoerModelDTO responseGoer = restTemplate.getForObject(
                goerUrl,
                GoerModelDTO.class);

        if (responseGoer==null)
            return null;

        //Appel au microservice relation
        String relationUrl = "https://relation-dot-festsharev2.appspot.com/relation/goer/" + idGoer + "/followEvent";

        String responseRelation = restTemplate.getForObject(
                relationUrl,
                String.class);

        try {
            //convert JSON array to List of objects
            List<RelationModelDTO> test2 = Arrays.asList(mapper.readValue(responseRelation, RelationModelDTO[].class));

            test2.forEach(x -> {
                Optional<EventModel> e = eventRepository.findById(x.getIdEntity());
                resultList.add(e.get());
            });
            return resultList;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public String removeEvent(String id) {
        try {
            Optional<EventModel> eventModel = eventRepository.findById(id);
            eventRepository.delete(eventModel.get());
            return id;
        } catch (Exception e) {
            return "error";
        }
    }
}

