package fr.cpe.jee2.event.repository;

import fr.cpe.jee2.event.EventStylesEnum;
import fr.cpe.jee2.event.model.EventModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EventRepository extends MongoRepository<EventModel, String> {

    List<EventModel> findByDateAndPlaceAndNameAndIdPromoterAndStyle(Date date, String place, String name, String idPromoter, EventStylesEnum style);

    List<EventModel> findByIdPromoter(String idPromoter);

    List<EventModel> findByNameContainingOrDescriptionContaining(String name, String description);
}
