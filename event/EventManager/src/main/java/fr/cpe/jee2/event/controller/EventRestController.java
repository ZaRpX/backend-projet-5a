package fr.cpe.jee2.event.controller;

import fr.cpe.jee2.event.model.EventModel;
import fr.cpe.jee2.event.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


//ONLY FOR TEST NEED ALSO TO ALLOW CROSS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class EventRestController {

	@Autowired
    private EventService eventService;

    @RequestMapping(method=RequestMethod.POST,value="/event")
    public ResponseEntity<String> postEvent(@RequestBody EventModel event) {
        String result = eventService.createEvent(event);

        switch (result) {
            case "error":
                return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
            case "alreadyExists":
                return new ResponseEntity<>("alreadyExists", HttpStatus.SEE_OTHER);
            case "empty":
                return new ResponseEntity<>("missing parameter", HttpStatus.NO_CONTENT);
            case "no promoter":
                return new ResponseEntity<>("not allowed", HttpStatus.FORBIDDEN);
            default:
                return new ResponseEntity<>(event.toString(), HttpStatus.CREATED);
        }
    }

    @RequestMapping(method=RequestMethod.PUT,value="/event")
    public ResponseEntity<String> putEvent(@RequestBody EventModel event) {
        String result = eventService.updateEvent(event);

        switch (result) {
            case "error":
                return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
            case "not found":
                return new ResponseEntity<>("alreadyExists", HttpStatus.SEE_OTHER);
            default:
                return new ResponseEntity<>(event.toString(), HttpStatus.OK);
        }
    }

    @RequestMapping("/event/{id}")
    private ResponseEntity<EventModel> getEvent(@PathVariable String id) {
        Optional<EventModel> event;
        event = eventService.getEvent(id);
        return new ResponseEntity<>(event.orElse(null), HttpStatus.OK);
    }

    @RequestMapping("/events")
    private ResponseEntity<List<EventModel>> getAllEvents() {
        return new ResponseEntity<>(eventService.getAllEvents(), HttpStatus.OK);
    }

    @RequestMapping("/events/search/{event}")
    private ResponseEntity<List<EventModel>> getEventsWithParam(@PathVariable String event) {
        return new ResponseEntity<>(eventService.getEventsWithParam(event), HttpStatus.OK);
    }

    @RequestMapping("/events/promoter/{idPromoter}")
    private ResponseEntity<List<EventModel>> getEventsByPromoter(@PathVariable("idPromoter") String idPromoter) {
        return new ResponseEntity<>(eventService.getEventsByPromoter(idPromoter), HttpStatus.OK);
    }

    @RequestMapping("/events/goer/{idGoer}")
    private ResponseEntity<List<EventModel>> getEventsByGoer(@PathVariable("idGoer") String idGoer) {
        List<EventModel> result;
        result = eventService.getEventsByGoer(idGoer);
        if (result == null) {
            return new ResponseEntity<>(null, HttpStatus.SEE_OTHER);
        } else {
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/event/{id}")
    public ResponseEntity<String> deleteEvent(@PathVariable String id) {
        String result = eventService.removeEvent(id);
        if (result.equals("error"))
            return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<>(id + "deleted", HttpStatus.OK);
    }
}
