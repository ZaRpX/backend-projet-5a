package fr.cpe.jee2.event.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.cpe.jee2.event.EventStylesEnum;
import fr.cpe.jee2.group.dto.GroupModelDTO;
import fr.cpe.jee2.promoter.dto.PromoterModelDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "event")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class EventModel implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private Date date;
	private Date dateFin;
	private String place;
	private String description;
	private String idPromoter;
	private EventStylesEnum style;
	private String name;
	private String bannerUrl;

	private transient List<GroupModelDTO> groupList = new ArrayList<>();
	private transient PromoterModelDTO promoter;

	public EventModel(Date date, Date dateFin, String place, String description, String idPromoter, EventStylesEnum style, String name, String bannerUrl) {
		this.date = date;
		this.dateFin = dateFin;
		this.place = place;
		this.description = description;
		this.idPromoter = idPromoter;
		this.style = style;
		this.name = name;
		this.bannerUrl = bannerUrl;
	}

	public EventModel() {}

	@Override
	public String toString() {
		return "EventModel{" +
				"id='" + id + '\'' +
				", date=" + date +
				", dateFin=" + dateFin +
				", place='" + place + '\'' +
				", description='" + description + '\'' +
				", idPromoter='" + idPromoter + '\'' +
				", style=" + style +
				", name='" + name + '\'' +
				", bannerUrl='" + bannerUrl + '\'' +
				", groupList=" + groupList +
				", promoter=" + promoter +
				'}';
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdPromoter() {
		return idPromoter;
	}

	public void setIdPromoter(String idPromoter) {
		this.idPromoter = idPromoter;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EventStylesEnum getStyle() {
		return style;
	}

	public void setStyle(EventStylesEnum style) {
		this.style = style;
	}

	public List<GroupModelDTO> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<GroupModelDTO> groupList) {
		this.groupList = groupList;
	}

	public PromoterModelDTO getPromoter() {
		return promoter;
	}

	public void setPromoter(PromoterModelDTO promoter) {
		this.promoter = promoter;
	}

	public String getBannerUrl() {
		return bannerUrl;
	}

	public void setBannerUrl(String bannerUrl) {
		this.bannerUrl = bannerUrl;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
}
