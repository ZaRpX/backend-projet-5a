package fr.cpe.jee2.event;

public enum EventStylesEnum {
    RAP, ROCK, ELECTRO, HARDCORE, JAZZ, CLASSIC, REGGAIE, TECHNO;
}
