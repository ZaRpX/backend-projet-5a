package fr.cpe.jee2.event.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.cpe.jee2.event.EventStylesEnum;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EventModelDTO implements Serializable {
    private String id;
    private Date date;
    private Date dateFin;
    private String place;
    private String description;
    private String idPromoter;
    private EventStylesEnum style;
    private String name;
    private String bannerUrl;

    public EventModelDTO(Date date, Date dateFin, String place, String description, String idPromoter, EventStylesEnum style, String name, String bannerUrl) {
        this.date = date;
        this.dateFin = dateFin;
        this.place = place;
        this.description = description;
        this.idPromoter = idPromoter;
        this.style = style;
        this.name = name;
        this.bannerUrl = bannerUrl;
    }

    public EventModelDTO() {}

    @Override
    public String toString() {
        return "EventModelDTO{" +
                "id='" + id + '\'' +
                ", date=" + date +
                ", dateFin=" + dateFin +
                ", place='" + place + '\'' +
                ", description='" + description + '\'' +
                ", idPromoter='" + idPromoter + '\'' +
                ", style=" + style +
                ", name='" + name + '\'' +
                ", bannerUrl='" + bannerUrl + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdPromoter() {
        return idPromoter;
    }

    public void setIdPromoter(String idPromoter) {
        this.idPromoter = idPromoter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventStylesEnum getStyle() {
        return style;
    }

    public void setStyle(EventStylesEnum style) {
        this.style = style;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }
}
