package com.mydeveloperplanet.mygoogleappengineplanet.mygoogleappengineplanet.controller;

import com.mydeveloperplanet.mygoogleappengineplanet.mygoogleappengineplanet.model.CommentModel;
import com.mydeveloperplanet.mygoogleappengineplanet.mygoogleappengineplanet.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
@RequestMapping(value = "/comment")
public class CommentRestController {

    @Autowired
    private CommentService commentService;

    @RequestMapping(method=RequestMethod.POST,value="/")
    public ResponseEntity<String> postComment(@RequestBody CommentModel comment) {
        String result = commentService.createComment(comment);
        if(result.equals("error"))
            return new ResponseEntity<>("error", HttpStatus.BAD_REQUEST);
        else
            return new ResponseEntity<>(comment.toString(), HttpStatus.OK);
    }

    @RequestMapping("/{id}")
    private ResponseEntity<CommentModel> getCommentById(@PathVariable String id) {
        Optional<CommentModel> comment;
        comment = commentService.getCommentById(id);
        return new ResponseEntity<>(comment.orElse(null), HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/{id}")
    public ResponseEntity<String> deleteComment(@PathVariable String id){
        String result = commentService.removeComment(id);
        if(result.equals("error"))
            return new ResponseEntity<>("error", HttpStatus.BAD_REQUEST);
        else
            return new ResponseEntity<>(id + "deleted", HttpStatus.OK);
    }

    @RequestMapping("/hello")
    public String hello() {
        StringBuilder message = new StringBuilder("Hello Comment Microservice !");
        try {
            InetAddress ip = InetAddress.getLocalHost();
            message.append(" From host: " + ip);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return message.toString();
    }

}
