package com.mydeveloperplanet.mygoogleappengineplanet.mygoogleappengineplanet.service;
import com.mydeveloperplanet.mygoogleappengineplanet.mygoogleappengineplanet.model.CommentModel;
import com.mydeveloperplanet.mygoogleappengineplanet.mygoogleappengineplanet.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    public String createComment( CommentModel comment) {
        try {
            commentRepository.save(comment);
            return comment.toString();
        } catch (Exception e) {
            return "error";
        }
    }

    public Optional<CommentModel> getCommentById(String id){
        return commentRepository.findById(id);
    }

    public String removeComment(String id) {
        try {
            Optional<CommentModel> commentModel = commentRepository.findById(id);
            commentRepository.delete(commentModel.get());
            return id;
        } catch (Exception e) {
            return "error";
        }
    }
}

