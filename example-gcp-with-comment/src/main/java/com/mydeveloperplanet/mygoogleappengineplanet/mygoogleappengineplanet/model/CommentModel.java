package com.mydeveloperplanet.mygoogleappengineplanet.mygoogleappengineplanet.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.cpe.jee2.comment.CommentTypesEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "comment")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class CommentModel implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	//id de l'auteur du message/commentaire
	private String idUser;
	//type du topic auquel le message est attaché
	private CommentTypesEnum type;
	//id du topic auquel le message est attaché
	private String idTopic;
	//Contenu textuel du message/commentaire
	private String content;

	//Objet non enregistré en base - lien vers l'event/group/post
	private transient Object topic;

	public CommentModel(String idUser, CommentTypesEnum type, String idTopic, String content) {
		this.idUser = idUser;
		this.type = type;
		this.idTopic = idTopic;
		this.content = content;
	}

	public CommentModel() {}

	@Override
	public String toString() {
		return "CommentModel{" +
				"id='" + id + '\'' +
				", idUser='" + idUser + '\'' +
				", type=" + type +
				", idTopic='" + idTopic + '\'' +
				", content='" + content + '\'' +
				", topic=" + topic +
				'}';
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdTopic() {
		return idTopic;
	}

	public void setIdTopic(String idTopic) {
		this.idTopic = idTopic;
	}

	public String getIdUser() {return idUser;}

	public void setIdUser(String idUser) {this.idUser = idUser;}

	public String getContent() {return content;}

	public void setContent(String content) {this.content = content;}

	public CommentTypesEnum getType() {
		return type;
	}

	public void setType(CommentTypesEnum type) {
		this.type = type;
	}

	public Object getTopic() {
		return topic;
	}

	public void setTopic(Object topic) {
		this.topic = topic;
	}
}
