package fr.cpe.jee2.promoter.controller;

import fr.cpe.jee2.promoter.model.PromoterModel;
import fr.cpe.jee2.promoter.service.PromoterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class PromoterRestController {

    @Autowired
    private PromoterService promoterService;

    @RequestMapping(method=RequestMethod.POST,value="/promoter")
    public ResponseEntity<String> postPromoter(@RequestBody PromoterModel promoter) {
        String result = promoterService.createPromoter(promoter);

        switch (result){
            case "error":
                return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
            case "wrong mail":
                return new ResponseEntity<>("wrong mail format", HttpStatus.NOT_ACCEPTABLE);
            case "empty":
                return new ResponseEntity<>("missing parameter", HttpStatus.NO_CONTENT);
            default:
                return new ResponseEntity<>(promoter.toString(), HttpStatus.CREATED);
        }
    }

    @RequestMapping("/promoter/{id}")
    private ResponseEntity<PromoterModel> getPromoterById(@PathVariable String id) {
        Optional<PromoterModel> promoter;
        promoter = promoterService.getPromoterById(id);
        return new ResponseEntity<>(promoter.orElse(null), HttpStatus.OK);
    }

    @RequestMapping(method=RequestMethod.DELETE,value="/promoter/{id}")
    public ResponseEntity<String> deletePromoter(@PathVariable String id){
        String result = promoterService.removePromoter(id);
        if(result.equals("error"))
            return new ResponseEntity<>("error", HttpStatus.INTERNAL_SERVER_ERROR);
        else
            return new ResponseEntity<>(id + "deleted", HttpStatus.OK);
    }

}
