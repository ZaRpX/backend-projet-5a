package fr.cpe.jee2.promoter.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.cpe.jee2.event.dto.EventModelDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "promoter")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class PromoterModel implements Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private String name;
	private String mail;
	private String logo;
	private String description;

	private transient List<EventModelDTO> eventList = new ArrayList<>();

	public PromoterModel(String name, String mail, String logo, String description) {
		this.name = name;
		this.mail = mail;
		this.logo = logo;
		this.description = description;
	}

	public PromoterModel() {}

	@Override
	public String toString() {
		return "PromoterModel{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", mail='" + mail + '\'' +
				", logo='" + logo + '\'' +
				", description='" + description + '\'' +
				", eventList=" + eventList +
				'}';
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<EventModelDTO> getEventList() {
		return eventList;
	}

	public void setEventList(List<EventModelDTO> eventList) {
		this.eventList = eventList;
	}
}
