package fr.cpe.jee2.promoter.service;
import fr.cpe.jee2.promoter.model.PromoterModel;
import fr.cpe.jee2.promoter.repository.PromoterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.Optional;


@Service
public class PromoterService {

    @Autowired
    private PromoterRepository promoterRepository;

    public String createPromoter(PromoterModel promoter) {

        //Contrôle de nom non vide
        if (promoter.getName().isEmpty())
            return "empty";

        //Contrôle du format de l'adresse mail
        try {
            InternetAddress emailAddr = new InternetAddress(promoter.getMail());
            emailAddr.validate();
        } catch (AddressException ex) {
            return "wrong mail";
        }

        //Contrôle de description non vide
        if (promoter.getDescription().isEmpty())
            return "empty";

        try {
            promoterRepository.save(promoter);
            return promoter.toString();
        } catch (Exception e) {
            return "error";
        }
    }

    public Optional<PromoterModel> getPromoterById(String id){
       return promoterRepository.findById(id);
    }

    public String removePromoter(String id) {
        try {
            Optional<PromoterModel> promoterModel = promoterRepository.findById(id);
            promoterRepository.delete(promoterModel.get());
            return id;
        } catch (Exception e) {
            return "error";
        }
    }
}

