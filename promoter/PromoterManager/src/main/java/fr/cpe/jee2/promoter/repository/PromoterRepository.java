package fr.cpe.jee2.promoter.repository;

import fr.cpe.jee2.promoter.model.PromoterModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PromoterRepository extends MongoRepository<PromoterModel, String> {

}
