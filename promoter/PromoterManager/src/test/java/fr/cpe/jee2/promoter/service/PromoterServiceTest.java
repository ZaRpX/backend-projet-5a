package fr.cpe.jee2.promoter.service;

import com.github.npathai.hamcrestopt.OptionalMatchers;
import fr.cpe.jee2.promoter.model.PromoterModel;
import fr.cpe.jee2.promoter.repository.PromoterRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PromoterServiceTest {

    @Mock
    PromoterRepository promoterRepository;

    @InjectMocks
    @Spy
    PromoterService promoterService;

    @Test
    public void createPromoter(){
        PromoterModel promoterModel = new PromoterModel();
        promoterModel.setName("");

        //On test la verification du nom
        Assert.assertEquals(promoterService.createPromoter(promoterModel), "empty");

        //On test la verification de l'adresse mail
        promoterModel.setName("test");
        promoterModel.setMail("test");
        Assert.assertEquals(promoterService.createPromoter(promoterModel), "wrong mail");

        //On test la verification de la description
        promoterModel.setMail("test@gmail.com");
        promoterModel.setDescription("");
        Assert.assertEquals(promoterService.createPromoter(promoterModel), "empty");

        //On vérifie que la méthode s'execute bien jusqu'au bout
        promoterModel.setDescription("test");
        Mockito.when(promoterRepository.save(promoterModel)).thenReturn(null);
        Assert.assertEquals(promoterService.createPromoter(promoterModel), promoterModel.toString());
    }

    @Test
    public void getPromoterById(){
        String id = "test";
        PromoterModel promoterModel = new PromoterModel();
        Mockito.when(promoterRepository.findById(id)).thenReturn(Optional.of(promoterModel));
        Assert.assertThat(promoterService.getPromoterById(id), OptionalMatchers.hasValue(promoterModel));
    }

    @Test
    public void removePromoter() {
        String id = "test";
        PromoterModel promoterModel = new PromoterModel();
        Mockito.when(promoterRepository.findById(id)).thenReturn(Optional.of(promoterModel));

        promoterService.removePromoter(id);
        Mockito.verify(promoterRepository, Mockito.times(1)).delete(promoterModel);
    }
}
