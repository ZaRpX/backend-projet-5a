package fr.cpe.jee2.promoter.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PromoterModelDTO implements Serializable {

    private String id;
    private String name;
    private String mail;
    private String logo;
    private String description;

    public PromoterModelDTO(String name, String mail, String logo, String description) {
        this.name = name;
        this.mail = mail;
        this.logo = logo;
        this.description = description;
    }

    public PromoterModelDTO() {}

    @Override
    public String toString() {
        return "PromoterModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", mail='" + mail + '\'' +
                ", logo='" + logo + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
